import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {


    @Override
    public void start(Stage stage) throws IOException {

        Stage stageImage = new Stage();
        Stage stageAllCaptor = new Stage();
        //Capteur
        CaptorBasique captor = new CaptorBasique("toto");
        CaptorZone capZone = new CaptorZone("totoZone");
        //Window
        PageWindow pageWindow = new PageWindow(captor);
        PageImage pageimage = new PageImage(captor);
        PageAllCaptor pageAllCaptor = new PageAllCaptor();
        //Create windows scene
        FXMLLoader loaderWindow =  new FXMLLoader(getClass().getResource("/FXML/PageWindow.fxml"));
        FXMLLoader loaderWindowImage =  new FXMLLoader(getClass().getResource("/FXML/PageImage.fxml"));
        FXMLLoader loaderWindowAllCaptor =  new FXMLLoader(getClass().getResource("/FXML/PageAllCaptor.fxml"));
        //------------------------------
        loaderWindow.setController(pageWindow);
        loaderWindowImage.setController(pageimage);
        loaderWindowAllCaptor.setController(pageAllCaptor);
        //------------------------------
        Parent page = loaderWindow.load();
        Parent pageImage = loaderWindowImage.load();
        Parent pageAllCaptorParent = loaderWindowAllCaptor.load();
        //------------------------------
        stage.setTitle("PageWindow");
        stageImage.setTitle("PageImage");
        stageAllCaptor.setTitle("Page all captor");
        //------------------------------
        Scene scene = new Scene(page);
        Scene sceneImage = new Scene(pageImage);
        Scene scenneAllCaptor = new Scene(pageAllCaptorParent);
        //Action
        captor.attach(pageWindow);
        captor.genValue("random");

        capZone.addCaptor(captor);

        pageAllCaptor.add(captor);
        pageAllCaptor.add(capZone);
        //Affichage
        stage.setScene(scene);
        stageImage.setScene(sceneImage);
        stageAllCaptor.setScene(scenneAllCaptor);
        stage.show();
        stageImage.show();
        stageAllCaptor.show();
    }
}


// FAIRE UNE LIST VIEW