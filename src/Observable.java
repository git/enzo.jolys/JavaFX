import java.util.ArrayList;
import java.util.List;

public abstract class Observable {

    private List<Oberserveur> listObserveur = new ArrayList<Oberserveur>();

    public void attach(Oberserveur o) {
        listObserveur.add(o);
    }

    public void dettach(Oberserveur o) {
        listObserveur.remove(o);
    }

    public void notifier() {

        for (Oberserveur o : listObserveur) {
            o.update();
        }
    }

}
