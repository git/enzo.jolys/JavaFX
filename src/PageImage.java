import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class PageImage extends CaptorMonitorWindow {

    @FXML
    ImageView imgView;

    @FXML
    public void initialize() {
        changementImage(temperature.doubleValue());

        temperature.addListener((observable,oldValue,newValue) -> {
            changementImage(newValue.doubleValue());
        });
    }

    public PageImage(Captor capt) {
        super(capt);
    }

    private void changementImage(Double value){
        if ( value <= 0.0 ){
            //l'URL n'est pas valide
           //imgView = new ImageView(new Image("/Ressources/Images/neige.jpg"));
        }
        else {
            //imgView = new ImageView(new Image("/Ressources/Images/soleil.jpg"));
        }
    }
}
