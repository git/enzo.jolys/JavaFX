import java.util.Objects;

public class CaptorBasique extends Captor implements Runnable {

    private GenStrategy genTemp;

    public CaptorBasique(String nom){
        this.nom = nom ;
        this.value = 0.0 ;
    }

    @Override
    public Double getValue() {
        return this.value;
    }

    public  void genValue(String opt){
        if (Objects.equals(opt, "random")) {
            genTemp = new GenRandom();
            run();
        } else if (Objects.equals(opt, "cpu")) {
            genTemp = new GenCpu();
            run();
        }
        if ( genTemp != null ){
            run();
        }
    }

    @Override
    public void run() {
            setValue(genTemp.generer());
            System.out.println(getValue());
    }
}
