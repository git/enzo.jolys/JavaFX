import java.util.Objects;

public abstract class Captor extends Observable {

    protected int id ;
    protected  String nom ;

    protected Double value ;

    public String getNom(){
        return this.nom ;
    }

    public void setValue(Double value){
        this.value = value;
        notifier();
        System.out.println(getValue());
    }

    public abstract Double getValue();

    public abstract void genValue(String opt) throws Exception;

}
