import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public abstract class CaptorMonitorWindow implements Oberserveur{

    public double getTemperature() {
        return temperature.get();
    }

    public DoubleProperty temperatureProperty() {
        return temperature;
    }

    public void setTemperature(double temperature) {

        //this.temperature.set(temperature);
        capt.setValue(temperature);
    }

    DoubleProperty temperature = new SimpleDoubleProperty();

    protected Captor capt;

    public CaptorMonitorWindow(Captor capt){
        this.capt = capt;
        setTemperature(0.0);
    }

    @Override
    public void update() {
        this.temperature.set(capt.getValue());
    }
}
