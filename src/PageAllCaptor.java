import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;


public class PageAllCaptor {

    @FXML
    ListView<Captor> listView = new ListView<>();

    @FXML
    public void initialize() {
        listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Captor>() {
            @Override
            public void changed(ObservableValue<? extends Captor> observableValue, Captor captor, Captor t1) {
                temp.setText("Température : "+t1.getValue());
            }
        });
    }

    @FXML
    Label temp ;

    public void add(Captor capt){
        listView.getItems().add(capt);
    }
}
