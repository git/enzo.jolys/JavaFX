import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class PageWindow extends CaptorMonitorWindow{


    @FXML
    private TextField temp ;

    @FXML
    public void initialize() {
        temp.textProperty().bind(temperature.asString());
        //Bindings.bindBidirectional((temp.textProperty(),temperature.asString()));
    }

    public PageWindow(Captor capt) {
        super(capt);
    }
}
