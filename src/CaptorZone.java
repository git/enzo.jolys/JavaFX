import java.util.ArrayList;
import java.util.List;

public class CaptorZone extends Captor {

    private List<Captor> listeCaptor = new ArrayList<>();

    public CaptorZone(String nom){
        this.nom = nom ;
        this.value = 0.0 ;
    }

    public void addCaptor(Captor captor){
        listeCaptor.add(captor);
    }

    public void deleteCaptor(Captor captor){
        listeCaptor.remove(captor);
    }

    @Override
    public Double getValue() {
        Double resultat = 0.0 ;
        int size ;

        if ( (size = listeCaptor.size()) == 0){
            return 0.0 ;
        }

        for ( Captor cap : listeCaptor) {
            resultat += cap.getValue();
        }
        return resultat/size;
    }

    @Override
    public void genValue(String opt) throws Exception {
        throw new Exception("Classe : CaptorZone ne contient pas la méthode !");
    }
}
